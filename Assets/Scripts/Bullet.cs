﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Bullet : MonoBehaviour
{
    private float speed = 20f;
    public Rigidbody2D rb;
    public int direction = 1;

    //bei Direction 1 nach Rechts
    void Start()
    {
        rb.velocity = transform.right * speed * direction;
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
        if(collision.tag == "Player")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void setDirection(bool right)
    {
        if (right)
        {
            direction = 1;
            rb.velocity = transform.right * speed * direction;
        }
        else
        {
            direction = -1;
            rb.velocity = transform.right * speed * direction;
        }
    }
}
