﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorController : MonoBehaviour
{
    private int level;

    private void Awake()
    {
        level = SceneManager.GetActiveScene().buildIndex;
    }


    void OnTriggerEnter2D(Collider2D coll) {
        if (coll.gameObject.tag != "Player") {
            return;
        }

        if (!coll.gameObject.GetComponent<PlayerController>().hasTablet) {
            return;
        }

        SceneManager.LoadScene("Level" + (level + 1));
    }
}
