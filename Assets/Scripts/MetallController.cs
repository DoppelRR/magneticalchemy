﻿using UnityEngine;

//Man muss Player playerTag geben und in Layer "Player" packen damit das läuft... sonst geht es
//Kann beim Draggen noch durch andere Objekte durch wie Player und Wall...buggt nach loslassen raus
public class MetallController : MonoBehaviour
{
    //Wie macht man das mit GetKomponent für PlayerReference 

    public bool dragged = false;

    private Rigidbody2D rb;
    private GameObject player;
    private float rayDistance = 10f;

    Vector3 mOffset;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

 
    
    private void OnMouseDown()
    {
        if (IsMoveable(rayDistance))
        {
            Vector3 mousePoint = Input.mousePosition;
            mOffset = transform.position - Camera.main.ScreenToWorldPoint(mousePoint);
            rb.gravityScale = 0;
            dragged = true;
        }
    }

    private void OnMouseUp() {
        rb.gravityScale = 1;
        dragged = false;
    }

    //move obj while pressing Mouse down and player in Distance
    private void OnMouseDrag()
    {
        if (dragged)
        {
            Vector3 mousePoint = Input.mousePosition;
            Vector3 newPos = Camera.main.ScreenToWorldPoint(mousePoint) + mOffset;
            Vector2 playerVector = player.transform.position - newPos;
            if (rayDistance < playerVector.magnitude) {
                return;
            }
            rb.MovePosition(newPos);
        }
    }

    //playerVector is Object to Player Vector
    //Checks if Distance is larger than length of this vector and then if the raycast hits the player
    bool IsMoveable(float distance)
    {
        if (player.GetComponent<PlayerController>().hasTablet) {
            return false;
        }

        Vector2 playerVector = player.transform.position - transform.position;
        if (distance < playerVector.magnitude) {
            return false;
        }

        RaycastHit2D hit = Physics2D.Linecast(rb.position, player.transform.position, 1 << LayerMask.NameToLayer("Player"));
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.CompareTag("Player")) {
                return true;
            }

        }
        return false;
    }

}