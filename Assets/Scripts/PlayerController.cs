﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : PhysicsObject {

    public Sprite spriteNoTablet;
    public Sprite spriteWithTablet;
    public GameObject tabletPrefab;

    public bool hasTablet = false;
    private bool paused = false;
    private int groundedSince = 0;

    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    public float maxPickupDistance;

    private SpriteRenderer sr;
    private GameObject canvas;
    private Animator animator;

    private void Awake() {
        canvas = GameObject.FindGameObjectWithTag("Pause");
        sr = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        canvas.SetActive(paused);
    }

    // Update is called once per frame
    void Update() {
        if (!grounded && hasTablet) {
            groundedSince++;
            if (groundedSince > 15)
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        } else {
            groundedSince = 0;
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            paused = !paused;
            Time.timeScale = paused ? 0 : 1;
            canvas.SetActive(paused);
        }
        if (paused) {
            return;
        }
        if (Input.GetKeyDown(KeyCode.LeftShift) && grounded && !animator.GetBool("Walking")) {
            if (hasTablet) {
                Vector3 tabPos = transform.position + new Vector3(sr.flipX ? -1 : 1, -0.75f);
                Collider2D hit = Physics2D.OverlapArea(tabPos + new Vector3(-0.375f, 0.1875f), tabPos + new Vector3(0.375f, -0.1875f), 1);
                if (hit == null) {
                    GameObject tablet = GameObject.Instantiate(tabletPrefab, tabPos, Quaternion.identity);
                    hasTablet = false;
                    sr.sprite = spriteNoTablet;
                    gravityModifier = 1;
                    animator.SetBool("HasTablet", false);
                } else {
                    tabPos = transform.position + new Vector3(sr.flipX ? -1 : 1, 0.25f);
                    hit = Physics2D.OverlapArea(tabPos + new Vector3(-0.375f, 0.1875f), tabPos + new Vector3(0.375f, -0.1875f), 1);
                    if (hit == null) {
                        GameObject tablet = GameObject.Instantiate(tabletPrefab, tabPos, Quaternion.identity);
                        hasTablet = false;
                        sr.sprite = spriteNoTablet;
                        gravityModifier = 1;
                        animator.SetBool("HasTablet", false);
                    }
                }
            } else {
                GameObject tablet = GameObject.FindGameObjectWithTag("Tablet");
                float distance = (transform.position - tablet.transform.position).magnitude;
                if (distance <= maxPickupDistance) {
                    sr.sprite = spriteWithTablet;
                    hasTablet = true;
                    Destroy(tablet);
                    gravityModifier = 10;
                    animator.SetBool("HasTablet", true);
                }
            }   
        }

        base.ParentUpdate();
    }

    protected override void ComputeVelocity() {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded) {
            velocity.y = jumpTakeOffSpeed;
        } else if (Input.GetButtonUp("Jump")) {
            if (velocity.y > 0) {
                velocity.y = velocity.y * 0.5f;
            }
        }

        bool flipSprite = (sr.flipX ? (move.x > 0.01f) : (move.x < -0.01f));
        if (flipSprite) {
            sr.flipX = !sr.flipX;
        }

        targetVelocity = move * maxSpeed;
        animator.SetBool("Walking", targetVelocity.x != 0);
        animator.SetBool("Jumping", !grounded);
    }


}
