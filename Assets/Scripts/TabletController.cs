﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TabletController : MonoBehaviour {

    private Rigidbody2D rb;

    public float fallExplodeThreshold;
    public float collisionExplodeThreshold;

    void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
    }

    void OnCollisionEnter2D(Collision2D c) {
        if (c.relativeVelocity.magnitude > collisionExplodeThreshold) {
            Destroy(gameObject);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
