﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartButtonClick() {
        SceneManager.LoadScene(1);
    }

    public void ExitButtonClick() {
        SceneManager.LoadScene(0);
    }

    public void ExitGameButtonClick() {
        Application.Quit();
    }
}
