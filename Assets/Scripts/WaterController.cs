﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class WaterController : MonoBehaviour
{
    //Restart
    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
