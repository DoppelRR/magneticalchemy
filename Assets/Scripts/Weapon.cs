﻿using UnityEngine;

public class Weapon : MonoBehaviour
{
    public bool shootRight = true;
    public Transform firePointRight;
    public Transform firePointLeft;
    public GameObject bulletPrefab;
    private float Counter = 30;
    

    void FixedUpdate()
    {
        Counter++;
        if (Counter == 60)
        {
            Shoot();
            Counter = 0;
        }

    }

    void Shoot()
    {
        if (shootRight)
        {
            bulletPrefab.GetComponent<Bullet>().setDirection(true);
            Instantiate(bulletPrefab, firePointRight.position, firePointRight.rotation);
        }
        else
        {

            bulletPrefab.GetComponent<Bullet>().setDirection(false);
            Instantiate(bulletPrefab, firePointLeft.position, firePointLeft.rotation);
 
            
        }
       
    }
}
